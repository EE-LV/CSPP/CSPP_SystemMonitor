﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str">CSPP_BuildContent,CSPP_Core;CSPP_WebpubLaunchBrowser,None;CSPP_DSC,True;CSPP_DIM,False;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project is based on NI ActorFramework and CS++ libraries.
It is used to publish CPU, Memory and Disc usage to Shared Variable or DIM services.

Please refer also to README.md.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2019  GSI Helmholtzzentrum für Schwerionenforschung GmbH

EEL, Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{073D6D98-FD92-461A-AA29-BCAA8812E25F}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{07C1CE4D-817D-4FC8-AE68-79B77D32FA3F}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingInterval</Property>
	<Property Name="varPersistentID:{0AC716B8-1555-45A0-8CD9-048AA335D1D2}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{0F265497-1ADF-4A7C-83BA-9CD140646A99}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{11CB3D27-4508-418B-B3AC-7435E09558C3}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Usage_D</Property>
	<Property Name="varPersistentID:{12EF969F-88A3-4BFB-A929-48A07272F1C7}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{18EC0880-4D2D-469C-8BA7-419CF04C68A7}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{19A9FF45-3CA1-427E-9130-90399EDF12B8}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{19BB3CE2-C961-4F2D-8FBE-32A736E3D87F}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{1AA012B3-3DC0-4FC6-9A7C-1B24B0E19992}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_ErrorCode</Property>
	<Property Name="varPersistentID:{1B253A70-07C4-4192-B850-4208E11BB86A}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_PollingMode</Property>
	<Property Name="varPersistentID:{1B2DD514-18AF-427E-90D2-AC95B8CD2DC0}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{1B8DA19E-103E-443F-8A7A-A23B45BB885C}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{1D172D7E-8CC2-4FB8-BF91-ED5ED1F5EB21}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{1D5B2B11-EE79-4968-BEA4-7EA8A5090938}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{1D89C5D5-50DE-4F86-AB16-7A56BC5C26E1}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Reset</Property>
	<Property Name="varPersistentID:{1E0CF618-1224-44BA-A3EA-0CD7F712DB2F}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{2286C215-DC2E-4EFB-9BDC-D179940DA18B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Size_C</Property>
	<Property Name="varPersistentID:{237B7295-8D96-4824-A430-6FCDE5597658}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{24B72CCA-BA16-4578-A4D9-0616AA1F269A}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{2A9DAEEC-1B86-4619-80C4-DE30FE19FE66}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{2D83DA7C-1803-41C4-9F64-3CB9E2D7F20C}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{338BCD7F-03AA-438C-B04D-4CE106EC84C8}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{33FA0B91-30AB-4D47-9455-F583B189FD83}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActorProxy_Activate</Property>
	<Property Name="varPersistentID:{3B840716-50F6-482B-8854-3EB04FE0B180}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_PollingIterations</Property>
	<Property Name="varPersistentID:{3D8D858B-21D8-4123-A63C-9A55D184E9D3}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{3F13A2F1-7E0F-4A04-820A-D2E653263036}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorCode</Property>
	<Property Name="varPersistentID:{442C1961-CE61-43D7-8AA9-7D8301983642}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{4922AF7E-5F1A-4CA0-809D-CA70B9DCD61B}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_PollingCounter</Property>
	<Property Name="varPersistentID:{494C369B-18DC-4E51-87A4-D791D8DC43EA}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Error</Property>
	<Property Name="varPersistentID:{4ADA9DF6-F8FC-4EE4-8CA0-83BFA5B2BBC3}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{4AE3CB8B-8F48-4C20-9C6A-129F75147B0A}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{4CC2E503-C069-416B-B98B-A5D6598DD8C3}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{4E5AA46B-3C5F-4E00-9F6F-F26334E489A7}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_PollingCounter</Property>
	<Property Name="varPersistentID:{4FD4DABA-BD83-429A-ABFE-311EDD60E3B7}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{532BEBCA-D1CD-4BDB-A67F-F79D47428512}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Free_D</Property>
	<Property Name="varPersistentID:{54F91193-4A26-499F-AA39-55048DF99420}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{552CCD0B-47BF-4D59-89B7-AEA4BE5A74D9}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_PollingIterations</Property>
	<Property Name="varPersistentID:{57552CF2-DC6F-4CB0-A9B4-22DF49F28265}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{5F545216-364E-4DA1-9939-5C7AE557C911}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{62B4AC8A-0890-4056-A1E2-FC8494482AC2}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Free_D</Property>
	<Property Name="varPersistentID:{6305ADF8-687B-4D00-9ECE-7945786C7DD4}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingCounter</Property>
	<Property Name="varPersistentID:{6405CCE4-2F84-4A41-8EFA-407290CFB465}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{64760E5F-B43F-45CA-A26D-F517E8FF2E39}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{651CDC7F-5CC7-416B-BBF1-B14E974006AD}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{68ED4284-C70F-4EB4-B827-09CF93C02179}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{6A861C2F-B264-46B3-8532-27D23ACCF05B}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_CPU-Load</Property>
	<Property Name="varPersistentID:{6B60F401-500D-4933-9BD4-F94F113C6E4E}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingMode</Property>
	<Property Name="varPersistentID:{6E49D7E6-1586-400E-8CB0-DDFDEB03FF23}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Usage_C</Property>
	<Property Name="varPersistentID:{70846165-A11A-4C39-AFA4-1D1C61041CF9}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{73382DF9-2A13-4284-BF43-FE72EC443048}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{733C6696-4CFD-4FFE-99C7-0EF9AEA2EC8C}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{749E5164-35AA-4182-845E-9DF3320DF5FF}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{7A691F85-6B5E-410B-9838-56129DAC0629}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{7EF7CA35-1EE3-4AE4-A3BA-7FF503831AE8}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_Error</Property>
	<Property Name="varPersistentID:{81264667-95B6-4810-9423-50FDFA3732C6}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_PollingTime</Property>
	<Property Name="varPersistentID:{81784FEB-B29E-41BF-885E-799C8CD6CD44}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{8766DCFB-F57B-475D-8C9A-76FD55665BC7}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/ActorList</Property>
	<Property Name="varPersistentID:{89D6FA3A-6684-4BEB-A346-CC2491E55F1C}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{8FB33DF4-96C4-41E8-B1E5-4FE3F1E48B6C}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{A2638220-8D15-4146-AC85-C8751FEC6F39}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Size_D</Property>
	<Property Name="varPersistentID:{A58FCF55-86B5-463D-8524-1E12482B080C}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{A99D47FA-3F64-417E-B320-15AEA2AC1508}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_PollingInterval</Property>
	<Property Name="varPersistentID:{ABFFF773-D2C1-4E71-9384-2892494F26FD}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{AE4C6028-78A8-4973-93F0-FC4D7842A20D}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActorProxy_Activate</Property>
	<Property Name="varPersistentID:{B0F8ADAA-9194-4250-9DC1-3E4D18CA3254}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{B152CE96-9529-4A15-962F-3ABD7240A44A}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_PollingMode</Property>
	<Property Name="varPersistentID:{B160D19F-CBB8-4784-84BC-2C1626983478}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{B1B12962-70C1-45A4-9D91-1C1E89D76617}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{B618C08F-B4F6-4AA1-96BA-6B0237B47CAB}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{B6A34524-18B9-45BA-ACA4-E11254F0E0F5}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{B6A6E0BE-B98A-47A9-B5C0-C0F28767FE23}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{BA70F4EC-126B-47F4-B1C6-E84206675C54}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingInterval</Property>
	<Property Name="varPersistentID:{BEB7A54B-7484-4442-940B-BEC3A69F060B}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{BFE3980E-A8D0-42B0-9AE4-1E41CA27EA5A}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitorProxy_Activate</Property>
	<Property Name="varPersistentID:{C399D542-7CDA-4BE9-9AE5-89750CA8CF61}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_SelfTest</Property>
	<Property Name="varPersistentID:{C4104D90-A158-4E5E-90E7-50EB778A121D}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{C6D67BDB-7F9D-4D19-9B1A-64AE38966D1B}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_PollingTime</Property>
	<Property Name="varPersistentID:{D09972C4-08A4-4BBD-A6BE-7895663C5602}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingTime</Property>
	<Property Name="varPersistentID:{D297EF33-9004-47B8-98F7-DBE0FB97A7FB}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Size_D</Property>
	<Property Name="varPersistentID:{D7132AED-C555-4027-9980-822FAC248276}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/StartActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{DEBD5C13-57F3-4C93-A53B-CB1F371B570C}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{E4112C03-1F4F-4F07-9175-27750AF7F08D}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Error</Property>
	<Property Name="varPersistentID:{E4716480-CD29-48CA-8C75-76EE3228D6A2}" Type="Ref">/My Computer/Packages/Utilities/CSPP_SystemMonitor_SV.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{E7294D3F-21FF-449C-A655-0ADD3939828E}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{EAB34D42-FD62-4F17-AD05-9EC746B36315}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Free_C</Property>
	<Property Name="varPersistentID:{F1DE7661-CCE8-41AA-B994-6B932F485EA0}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Memory</Property>
	<Property Name="varPersistentID:{F6C61B8C-D0CC-43C1-9BF5-4512C66EC30C}" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib/SystemMonitor_Usage_D</Property>
	<Property Name="varPersistentID:{FFD09BB7-7C90-463E-A2CA-A824C9B7149D}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingDeltaT</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="AF" Type="Folder">
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
			<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
		</Item>
		<Item Name="Documentation" Type="Folder"/>
		<Item Name="EUPL License" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
		</Item>
		<Item Name="instr.lib" Type="Folder"/>
		<Item Name="Packages" Type="Folder">
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
					<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
					<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_Core/Change_Log.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_Core/Release_Notes.txt"/>
					<Item Name="VI-Analyzer-Configuration.cfg" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Configuration.cfg"/>
					<Item Name="VI-Analyzer-Results.rsl" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Results.rsl"/>
					<Item Name="VI-Analyzer-Spelling-Exceptions.txt" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Spelling-Exceptions.txt"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_NAInitialized Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_NAInitialized Msg/CSPP_NAInitialized Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
					<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core-errors.txt" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core-errors.txt"/>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_Core_SV.lvlib" Type="Library" URL="../Packages/CSPP_Core/CSPP_Core_SV.lvlib"/>
				<Item Name="CSPP_CoreContent-Linux.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent-Linux.vi"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
				<Item Name="CSPP_Post-Build Action.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_Post-Build Action.vi"/>
			</Item>
			<Item Name="DIM" Type="Folder">
				<Item Name="CS_CommandMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DIM/CS_CommandMonitor/CS_CommandMonitor.lvlib"/>
				<Item Name="CSPP_CS.lvlib" Type="Library" URL="../Packages/CSPP_DIM/CSConnection/CSPP_CS.lvlib"/>
				<Item Name="CSPP_DIM.ini" Type="Document" URL="../Packages/CSPP_DIM/CSPP_DIM.ini"/>
				<Item Name="CSPP_DIM.lvlib" Type="Library" URL="../Packages/CSPP_DIM/DIMConnection/CSPP_DIM.lvlib"/>
				<Item Name="CSPP_DIM_PerfClient.lvlib" Type="Library" URL="../Packages/CSPP_DIM/Examples/CSPP_DIM_PerfClient/CSPP_DIM_PerfClient.lvlib"/>
				<Item Name="CSPP_DIM_PerfServer.lvlib" Type="Library" URL="../Packages/CSPP_DIM/Examples/CSPP_DIM_PerfServer/CSPP_DIM_PerfServer.lvlib"/>
				<Item Name="CSPP_DIM_TypeServer.lvlib" Type="Library" URL="../Packages/CSPP_DIM/Examples/CSPP_DIM_TypeServer/CSPP_DIM_TypeServer.lvlib"/>
				<Item Name="CSPP_DIMContent.vi" Type="VI" URL="../Packages/CSPP_DIM/CSPP_DIMContent.vi"/>
				<Item Name="CSPP_DIMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DIM/DIM_Monitor/CSPP_DIMMonitor.lvlib"/>
				<Item Name="DimIndicators.lvlib" Type="Library" URL="../Packages/DimLVEvent/DimIndicators/DimIndicators.lvlib"/>
				<Item Name="LVDimInterface.lvlib" Type="Library" URL="../Packages/DimLVEvent/LVDimInterface/LVDimInterface.lvlib"/>
				<Item Name="README.txt" Type="Document" URL="../Packages/CSPP_DIM/README.txt"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
				<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
				<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
				<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
				<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
				<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				<Item Name="CSPP_DSCUtilities.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Libs/CSPP_DSCUtilities/CSPP_DSCUtilities.lvlib"/>
				<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
			</Item>
			<Item Name="Utilities" Type="Folder">
				<Item Name="CSPP_BeepActor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_BeepActor/CSPP_BeepActor.lvlib"/>
				<Item Name="CSPP_SystemMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor.lvlib"/>
				<Item Name="CSPP_SystemMonitor_SV.lvlib" Type="Library" URL="../Packages/CSPP_Utilities/Actors/CSPP_SystemMonitor/CSPP_SystemMonitor_SV.lvlib"/>
				<Item Name="CSPP_Utilities.ini" Type="Document" URL="../Packages/CSPP_Utilities/CSPP_Utilities.ini"/>
				<Item Name="CSPP_UtilitiesContent.vi" Type="VI" URL="../Packages/CSPP_Utilities/CSPP_UtilitiesContent.vi"/>
			</Item>
		</Item>
		<Item Name="SV.lib" Type="Folder"/>
		<Item Name="User" Type="Folder"/>
		<Item Name="CSPP-SystemMonitor.ini" Type="Document" URL="../CSPP-SystemMonitor.ini"/>
		<Item Name="CSPP-SystemMonitor.lvlib" Type="Library" URL="../SV.lib/CSPP-SystemMonitor.lvlib"/>
		<Item Name="CSPP-SystemMonitor_Content.vi" Type="VI" URL="../CSPP-SystemMonitor_Content.vi"/>
		<Item Name="CSPP-SystemMonitor_Main.vi" Type="VI" URL="../CSPP-SystemMonitor_Main.vi"/>
		<Item Name="CSPP_SystemMonitor.ico" Type="Document" URL="../CSPP_SystemMonitor.ico"/>
		<Item Name="README.md" Type="Document" URL="../README.md"/>
		<Item Name="Release_Notes.md" Type="Document" URL="../Release_Notes.md"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Stall Data Flow.vim" Type="VI" URL="/&lt;vilib&gt;/Utility/Stall Data Flow.vim"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CSPP-SystemMonitor" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{773E39E1-BBD6-4173-9460-6FE7C7522091}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A41324B4-1D83-44D0-AB6D-155F8178B03C}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/CSPP-SystemMonitor.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="App_winsec.certificate" Type="Str">Brand Holger (Brand)</Property>
				<Property Name="App_winsec.description" Type="Str">http://timestamp.verisign.com</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.verisign.com/scripts/timstamp.dll</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{95E968AE-FBA3-4AF4-B59D-842A4FF56C3A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/builds/CSPPDSC-SystemMonitor/App</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Packages/Core/CSPP_Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{6211658F-9962-4EFC-81EE-AF45E75227E4}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/D/builds/CSPP-Template/App/CSPP-Template_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">24</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP-SystemMonitor.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/builds/CSPPDSC-SystemMonitor/App/NI_AB_PROJECTNAME.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/builds/CSPPDSC-SystemMonitor/App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP_SystemMonitor.ico</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{27F0AB0D-EA6A-4418-9E2B-1A695F600D80}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/EUPL License/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/msvcp100.dll</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/msvcr100.dll</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/DimStd.dll</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/libDimWrapper.dll</Property>
				<Property Name="Source[14].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/libDimWrapperSPL.dll</Property>
				<Property Name="Source[15].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/Packages/DIM/README.txt</Property>
				<Property Name="Source[16].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/CSPP-SystemMonitor_Main.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Release_Notes.md</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Packages/DSC/CSPP_DSC.ini</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager.ini</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities.ini</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core.ini</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/EUPL License/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">17</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP-SystemMonitor publishes CPU, Memory and Disc usage.</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{20017911-8940-4421-B57F-6FCEEB0905B6}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP-SystemMonitor.exe</Property>
			</Item>
			<Item Name="CSPP-SystemMonitor Installer" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{6C2EA93A-BE4B-4929-BC67-ECE3DC942BFB}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI DataSocket 19.0</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{81A7E53E-9524-41CE-90D3-7DD3D90B6C58}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{E60B4861-89AB-4E60-96C2-93AB25CC9AE4}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Distributed System Manager 2019</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{CEF5E531-69E2-461E-8628-0998E4DD0317}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C02E174E-A57A-4B5F-A762-8FCA4854370A}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW Remote Execution Support 2019</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{1DB06C72-60F2-48DC-BAEA-935A3CFFFA3C}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[3].productID" Type="Str">{FA0DB08E-BC18-4194-9ADC-026B7C8D5CEA}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI Variable Engine 2019</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[4].productID" Type="Str">{EE27B7AE-EC56-49EC-9153-7D4CE64EDCA2}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI LabVIEW Runtime 2019 f2</Property>
				<Property Name="DistPart[4].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[4].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[4].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[1].productName" Type="Str">NI Deployment Framework 2019</Property>
				<Property Name="DistPart[4].SoftDep[1].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[4].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[10].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[4].SoftDep[10].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[4].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[11].productName" Type="Str">NI TDM Streaming 19.0</Property>
				<Property Name="DistPart[4].SoftDep[11].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[4].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[2].productName" Type="Str">NI Error Reporting 2019</Property>
				<Property Name="DistPart[4].SoftDep[2].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[4].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2019</Property>
				<Property Name="DistPart[4].SoftDep[3].upgradeCode" Type="Str">{8386B074-C90C-43A8-99F2-203BAAB4111C}</Property>
				<Property Name="DistPart[4].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[4].productName" Type="Str">NI LabVIEW Runtime 2019 Non-English Support.</Property>
				<Property Name="DistPart[4].SoftDep[4].upgradeCode" Type="Str">{446D49A5-F830-4ADF-8C78-F03284D6882D}</Property>
				<Property Name="DistPart[4].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[5].productName" Type="Str">NI Logos 19.0</Property>
				<Property Name="DistPart[4].SoftDep[5].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[4].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2019</Property>
				<Property Name="DistPart[4].SoftDep[6].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[4].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[7].productName" Type="Str">NI mDNS Responder 19.0</Property>
				<Property Name="DistPart[4].SoftDep[7].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[4].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[4].SoftDep[8].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[4].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[9].productName" Type="Str">Math Kernel Libraries 2018</Property>
				<Property Name="DistPart[4].SoftDep[9].upgradeCode" Type="Str">{33A780B9-8BDE-4A3A-9672-24778EFBEFC4}</Property>
				<Property Name="DistPart[4].SoftDepCount" Type="Int">12</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{7D6295E5-8FB8-4BCE-B1CD-B5B396FA1D3F}</Property>
				<Property Name="DistPartCount" Type="Int">5</Property>
				<Property Name="INST_author" Type="Str">GSI GmbH (DE)</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/D/builds/CSPPDSC-SystemMonitor/Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">CSPP-SystemMonitor Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="INST_installerName" Type="Str">install.exe</Property>
				<Property Name="INST_productName" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="INST_productVersion" Type="Str">0.0.0</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">19008009</Property>
				<Property Name="MSI_arpCompany" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">H.Brand@gsi.de, D.Neidherr@gsi.de</Property>
				<Property Name="MSI_arpPhone" Type="Str">2123, 1885</Property>
				<Property Name="MSI_arpURL" Type="Str">https://www.gsi.de</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{524578CA-8ECA-4356-AF4B-A5803259C115}</Property>
				<Property Name="MSI_hideNonRuntimes" Type="Bool">true</Property>
				<Property Name="MSI_licenseID" Type="Ref">/My Computer/EUPL License/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{19C42E4C-FBB1-4E46-8829-77953DD37C75}</Property>
				<Property Name="MSI_windowMessage" Type="Str">This application will read and publish CPU, Memory and Disk usage to Shared Variables or DIM Services (default). Do not forget to set enviroment variable DIM_DNS_NODE where the dns.exe is executed.</Property>
				<Property Name="MSI_windowTitle" Type="Str">CSPP SystemMonitor</Property>
				<Property Name="MSI_winsec.certificate" Type="Str">Brand Holger (Brand)</Property>
				<Property Name="MSI_winsec.description" Type="Str">http://timestamp.verisign.com</Property>
				<Property Name="MSI_winsec.timestamp" Type="Str">http://timestamp.verisign.com/scripts/timstamp.dll</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">CSPP-SystemMonitor.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{20017911-8940-4421-B57F-6FCEEB0905B6}</Property>
				<Property Name="Source[0].File[1].attributes" Type="Int">1</Property>
				<Property Name="Source[0].File[1].dest" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Source[0].File[1].name" Type="Str">CSPP-SystemMonitor.ini</Property>
				<Property Name="Source[0].File[1].tag" Type="Str">{A41324B4-1D83-44D0-AB6D-155F8178B03C}</Property>
				<Property Name="Source[0].FileCount" Type="Int">2</Property>
				<Property Name="Source[0].name" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/CSPP-SystemMonitor</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
			<Item Name="CSPP-SystemMonitor-DIM" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{EDCA642B-F1C3-45CC-873B-A9CB395B57D8}</Property>
				<Property Name="App_INI_GUID" Type="Str">{AF0CA883-93D3-4270-9F53-64E00E3F869B}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/CSPP-SystemMonitor.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_useFFRTE" Type="Bool">true</Property>
				<Property Name="App_winsec.certificate" Type="Str">Brand Holger (Brand)</Property>
				<Property Name="App_winsec.description" Type="Str">http://timestamp.verisign.com</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.verisign.com/scripts/timstamp.dll</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{2AA20B12-7493-4F48-B8FA-DA92EAA57167}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP-SystemMonitor-DIM</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/D/builds/CSPPDIM-SystemMonitor/App</Property>
				<Property Name="Bld_postActionVIID" Type="Ref">/My Computer/Packages/Core/CSPP_Post-Build Action.vi</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{4260C719-5981-47BF-84F4-B7726329075F}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">/D/builds/CSPP-Template/App/CSPP-Template_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">24</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP-SystemMonitor.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/D/builds/CSPPDIM-SystemMonitor/App/CSPP-SystemMonitor-DIM.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/D/builds/CSPPDIM-SystemMonitor/App/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP_SystemMonitor.ico</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{A43D3BAB-CE46-4C26-AF80-ECDD0A164A4E}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/EUPL License/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/msvcp100.dll</Property>
				<Property Name="Source[11].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/msvcr100.dll</Property>
				<Property Name="Source[12].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/DimStd.dll</Property>
				<Property Name="Source[13].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/libDimWrapper.dll</Property>
				<Property Name="Source[14].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/Packages/DIM/LVDimInterface.lvlib/supportFiles/libDimWrapperSPL.dll</Property>
				<Property Name="Source[15].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[16].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[16].itemID" Type="Ref">/My Computer/Packages/DIM/README.txt</Property>
				<Property Name="Source[16].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[17].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[17].itemID" Type="Ref">/My Computer/Packages/DIM/CSPP_DIM.ini</Property>
				<Property Name="Source[17].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/CSPP-SystemMonitor_Main.vi</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">VI</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Release_Notes.md</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Packages/DSC/CSPP_DSC.ini</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager.ini</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Packages/Utilities/CSPP_Utilities.ini</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core.ini</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">1</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/CSPP-SystemMonitor.lvlib</Property>
				<Property Name="Source[8].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].type" Type="Str">Library</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/EUPL License/EUPL v.1.1 - Lizenz.pdf</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">18</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP-SystemMonitor publishes CPU, Memory and Disc usage. with DIM</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{4CE4261D-BA9F-4149-BF46-B1F0CDCD195E}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP-SystemMonitor.exe</Property>
			</Item>
			<Item Name="CSPP-SystemMonitor-DIM Installer" Type="Installer">
				<Property Name="AB_Class_Path" Type="Path">/C/Program Files (x86)/National Instruments/LabVIEW 2019/vi.lib/AppBuilder/IB_Classes/MSI/IB_MSI.lvclass</Property>
				<Property Name="AB_Temp_Project_Path" Type="Path">/C/User/Brand/LVP/CSPP/CSPP_SystemManager/CSPP-SystemMonitor.lvproj</Property>
				<Property Name="AB_UIClass_Path" Type="Path">/C/Program Files (x86)/National Instruments/LabVIEW 2019/vi.lib/AppBuilder/IB_Classes/Installer/UI/AB_UI_Frmwk_Installer.lvclass</Property>
				<Property Name="Destination[0].name" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="DistPart[0].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[0].productID" Type="Str">{6C2EA93A-BE4B-4929-BC67-ECE3DC942BFB}</Property>
				<Property Name="DistPart[0].productName" Type="Str">NI DataSocket 19.0</Property>
				<Property Name="DistPart[0].upgradeCode" Type="Str">{81A7E53E-9524-41CE-90D3-7DD3D90B6C58}</Property>
				<Property Name="DistPart[1].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[1].productID" Type="Str">{E60B4861-89AB-4E60-96C2-93AB25CC9AE4}</Property>
				<Property Name="DistPart[1].productName" Type="Str">NI Distributed System Manager 2019</Property>
				<Property Name="DistPart[1].upgradeCode" Type="Str">{CEF5E531-69E2-461E-8628-0998E4DD0317}</Property>
				<Property Name="DistPart[2].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[2].productID" Type="Str">{C02E174E-A57A-4B5F-A762-8FCA4854370A}</Property>
				<Property Name="DistPart[2].productName" Type="Str">NI LabVIEW Remote Execution Support 2019</Property>
				<Property Name="DistPart[2].upgradeCode" Type="Str">{1DB06C72-60F2-48DC-BAEA-935A3CFFFA3C}</Property>
				<Property Name="DistPart[3].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[3].productID" Type="Str">{FA0DB08E-BC18-4194-9ADC-026B7C8D5CEA}</Property>
				<Property Name="DistPart[3].productName" Type="Str">NI Variable Engine 2019</Property>
				<Property Name="DistPart[3].upgradeCode" Type="Str">{EB7A3C81-1C0F-4495-8CE5-0A427E4E6285}</Property>
				<Property Name="DistPart[4].flavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPart[4].productID" Type="Str">{EE27B7AE-EC56-49EC-9153-7D4CE64EDCA2}</Property>
				<Property Name="DistPart[4].productName" Type="Str">NI LabVIEW Runtime 2019 f2</Property>
				<Property Name="DistPart[4].SoftDep[0].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[0].productName" Type="Str">NI ActiveX Container</Property>
				<Property Name="DistPart[4].SoftDep[0].upgradeCode" Type="Str">{1038A887-23E1-4289-B0BD-0C4B83C6BA21}</Property>
				<Property Name="DistPart[4].SoftDep[1].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[1].productName" Type="Str">NI Deployment Framework 2019</Property>
				<Property Name="DistPart[4].SoftDep[1].upgradeCode" Type="Str">{838942E4-B73C-492E-81A3-AA1E291FD0DC}</Property>
				<Property Name="DistPart[4].SoftDep[10].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[10].productName" Type="Str">NI VC2015 Runtime</Property>
				<Property Name="DistPart[4].SoftDep[10].upgradeCode" Type="Str">{D42E7BAE-6589-4570-B6A3-3E28889392E7}</Property>
				<Property Name="DistPart[4].SoftDep[11].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[11].productName" Type="Str">NI TDM Streaming 19.0</Property>
				<Property Name="DistPart[4].SoftDep[11].upgradeCode" Type="Str">{4CD11BE6-6BB7-4082-8A27-C13771BC309B}</Property>
				<Property Name="DistPart[4].SoftDep[2].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[2].productName" Type="Str">NI Error Reporting 2019</Property>
				<Property Name="DistPart[4].SoftDep[2].upgradeCode" Type="Str">{42E818C6-2B08-4DE7-BD91-B0FD704C119A}</Property>
				<Property Name="DistPart[4].SoftDep[3].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[3].productName" Type="Str">NI LabVIEW Real-Time NBFifo 2019</Property>
				<Property Name="DistPart[4].SoftDep[3].upgradeCode" Type="Str">{8386B074-C90C-43A8-99F2-203BAAB4111C}</Property>
				<Property Name="DistPart[4].SoftDep[4].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[4].productName" Type="Str">NI LabVIEW Runtime 2019 Non-English Support.</Property>
				<Property Name="DistPart[4].SoftDep[4].upgradeCode" Type="Str">{446D49A5-F830-4ADF-8C78-F03284D6882D}</Property>
				<Property Name="DistPart[4].SoftDep[5].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[5].productName" Type="Str">NI Logos 19.0</Property>
				<Property Name="DistPart[4].SoftDep[5].upgradeCode" Type="Str">{5E4A4CE3-4D06-11D4-8B22-006008C16337}</Property>
				<Property Name="DistPart[4].SoftDep[6].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[6].productName" Type="Str">NI LabVIEW Web Server 2019</Property>
				<Property Name="DistPart[4].SoftDep[6].upgradeCode" Type="Str">{0960380B-EA86-4E0C-8B57-14CD8CCF2C15}</Property>
				<Property Name="DistPart[4].SoftDep[7].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[7].productName" Type="Str">NI mDNS Responder 19.0</Property>
				<Property Name="DistPart[4].SoftDep[7].upgradeCode" Type="Str">{9607874B-4BB3-42CB-B450-A2F5EF60BA3B}</Property>
				<Property Name="DistPart[4].SoftDep[8].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[8].productName" Type="Str">Math Kernel Libraries 2017</Property>
				<Property Name="DistPart[4].SoftDep[8].upgradeCode" Type="Str">{699C1AC5-2CF2-4745-9674-B19536EBA8A3}</Property>
				<Property Name="DistPart[4].SoftDep[9].exclude" Type="Bool">false</Property>
				<Property Name="DistPart[4].SoftDep[9].productName" Type="Str">Math Kernel Libraries 2018</Property>
				<Property Name="DistPart[4].SoftDep[9].upgradeCode" Type="Str">{33A780B9-8BDE-4A3A-9672-24778EFBEFC4}</Property>
				<Property Name="DistPart[4].SoftDepCount" Type="Int">12</Property>
				<Property Name="DistPart[4].upgradeCode" Type="Str">{7D6295E5-8FB8-4BCE-B1CD-B5B396FA1D3F}</Property>
				<Property Name="DistPartCount" Type="Int">5</Property>
				<Property Name="INST_author" Type="Str">GSI GmbH (DE)</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">/D/builds/CSPPDIM-SystemMonitor/Installer</Property>
				<Property Name="INST_buildSpecName" Type="Str">CSPP-SystemMonitor-DIM Installer</Property>
				<Property Name="INST_defaultDir" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="INST_installerName" Type="Str">install.exe</Property>
				<Property Name="INST_productName" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="INST_productVersion" Type="Str">0.0.0</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">19008009</Property>
				<Property Name="MSI_arpCompany" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="MSI_arpContact" Type="Str">H.Brand@gsi.de, D.Neidherr@gsi.de</Property>
				<Property Name="MSI_arpPhone" Type="Str">2123, 1885</Property>
				<Property Name="MSI_arpURL" Type="Str">https://www.gsi.de</Property>
				<Property Name="MSI_autoselectDrivers" Type="Bool">true</Property>
				<Property Name="MSI_distID" Type="Str">{3C1FDF94-33FB-4FC9-B3B9-ADF367D4A360}</Property>
				<Property Name="MSI_licenseID" Type="Ref">/My Computer/EUPL License/EUPL v.1.1 - Lizenz.rtf</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{CCA7D5E8-F911-4DCC-8B49-18F97C6798E5}</Property>
				<Property Name="MSI_windowMessage" Type="Str">This application will read and publish CPU, Memory and Disk usage to Shared Variables or DIM Services (default). Do not forget to set enviroment variable DIM_DNS_NODE where the dns.exe is executed.</Property>
				<Property Name="MSI_windowTitle" Type="Str">CSPP SystemMonitor</Property>
				<Property Name="MSI_winsec.certificate" Type="Str">Brand Holger (Brand)</Property>
				<Property Name="MSI_winsec.description" Type="Str">http://timestamp.verisign.com</Property>
				<Property Name="MSI_winsec.timestamp" Type="Str">http://timestamp.verisign.com/scripts/timstamp.dll</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">CSPP-SystemMonitor.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{20017911-8940-4421-B57F-6FCEEB0905B6}</Property>
				<Property Name="Source[0].File[1].attributes" Type="Int">1</Property>
				<Property Name="Source[0].File[1].dest" Type="Str">{BB300358-5ED9-417E-A50B-9F27AB07DDFD}</Property>
				<Property Name="Source[0].File[1].name" Type="Str">CSPP-SystemMonitor.ini</Property>
				<Property Name="Source[0].File[1].tag" Type="Str">{A41324B4-1D83-44D0-AB6D-155F8178B03C}</Property>
				<Property Name="Source[0].FileCount" Type="Int">2</Property>
				<Property Name="Source[0].name" Type="Str">CSPP-SystemMonitor</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/CSPP-SystemMonitor</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="SourceCount" Type="Int">1</Property>
			</Item>
		</Item>
	</Item>
</Project>
